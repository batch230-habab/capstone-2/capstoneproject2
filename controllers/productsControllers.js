const mongoose = require("mongoose");
const products = require("../models/products.js");
const bcrypt  = require("bcrypt");
const auth = require ("../auth.js");

module.exports.addProduct = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        let newProduct = new products({
            prodName: req.body.prodName,
            prodDesc: req.body.prodDesc,
            price: req.body.price,
            stockAmount: req.body.stockAmount
        })
        newProduct.save()
        .then(result => res.send(result))
        .catch(error => res.send(error));
    }else{
        res.send(`User doesn't have Admin privileges`);
    }
}

module.exports.getAllActive = (req, res) =>{
    products.find({isAvailable: true})
    .then(result => res.send(result))
    .catch(error => res.send(error));
}

module.exports.getProduct = (req, res) => {
    products.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.updateProduct = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        products.findByIdAndUpdate(req.params.productId, {
            prodName: req.body.prodName,
            prodDesc: req.body.prodDesc,
            price: req.body.price
        }, {new: true})
        .then(result => res.send(`product updated: \n${result}`))
        .catch(error => res.send(error));
    }else{
        res.send(`Must be an admin to access this function`)
    }
}

module.exports.archiveProduct = (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        products.findByIdAndUpdate(req.params.productId, {
            isAvailable: false
        }, {new: true})
        .then(result => res.send(`${result.prodName} is no longer available!`))
        .catch(error => res.send(error));
    }else{
        res.send(`Must be an admin to access this function`)
    }
}