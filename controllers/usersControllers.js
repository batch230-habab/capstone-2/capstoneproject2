const users = require("../models/users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const products = require("../models/products");

module.exports.registerUsersController = (req, res) =>{
    let newUser = new users({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: bcrypt.hashSync(req.body.password, 10),
        isAdmin: req.body.isAdmin
    })
    users.find().or([{email: req.body.email},{mobileNo:req.body.mobileNo}])
    .then(result =>{
        if(result.length != ""){
            res.send(`email/phone number already in use`)
        }else{
            newUser.save();
            res.send(newUser);
        }
    })
    .catch(error=>res.send(error))
}

module.exports.loginUser = (req, res) => {
    users.findOne({email:req.body.email})
    .then(result =>{
        if (result == null){
            console.log("login failed");
        }else{
            const isPassCorrect = bcrypt.compareSync(req.body.password, result.password);
            if(isPassCorrect){
                res.send({access: auth.createAccessToken(result)});
            }else{
                false;
            }
        }
    })
    .catch(error => res.send(error));
}

module.exports.getUserDetails = (req, res) => {
    users.findById(auth.decode(req.headers.authorization).id)
    .then(result =>{
        result.password = "*********";
        res.send(result);
    })
    .catch(error => res.send(error))
}

module.exports.addToCart = async(req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    let productToCart = await products.findById(req.body.productId).then(result => result);
    let receiptData = {
        userId: userData.id,
        email: userData.email,
        productId: req.body.productId,
        prodName: productToCart.prodName,
        stockAmount: productToCart.stockAmount
        /* firstName: userData.firstName, */
    }
    console.log(`preparing ${productToCart.prodName}`)
    let isUserUpdated = await users.findById(receiptData.userId)
    .then(result => {
        if(receiptData.stockAmount > 0){
            result.orders.push({
                productId: receiptData.productId,
                prodName: receiptData.prodName
            })
            return result.save()
            .then(saveResult => {
                console.log(saveResult);
                return true;
            })
            .catch(err=> {console.log(err);
            return false;})
        }else{
            console.log(`out of stock`);
            return false;
        }
    })
    console.log(`Your order`)
    let isProdUpdated = await products.findById(receiptData.productId)
    .then(prod => {
        prod.orders.push({
            userId: receiptData.userId,
            email: receiptData.email
        })
        if(prod.stockAmount >=1){
            prod.stockAmount -=1
            return prod.save()
            .then(result =>{
                console.log(`${result.prodName} has been added to cart`)
                return true;
            })
        }else{
            console.log(`out of stock`);
            return false;
        }
    })
    .catch(err=>console.log(err))
    console.log(isProdUpdated);
    (isUserUpdated == true && isProdUpdated == true)? res.send(true) : res.send(false);
}

module.exports.finalCheckout = async (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    let receiptData = {
        userId: userData.id,
        email: userData.email
    }
    await users.findById(receiptData.userId)
    .then(result => {
        console.log(`checking out items:\n ${result.orders}`)
        result.orders.splice(0, result.orders.length);
        console.log(`please proceed to payment`)
        console.log(result)
        result.save();
        res.send(`please proceed to payment`)
    })
    .catch(err=>console.log(err))
}