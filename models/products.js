const mongoose = require("mongoose");
const productsSchema = new mongoose.Schema(
    {
        prodName: {type: String, required: [true, "product name is required"]},
        prodDesc: {type: String, required: [true, "product description is required"]},
        price: {type: Number, required: [true, "product price is required"]},
        isAvailable: {type: Boolean, default: true},
        stockAmount: {type: Number, required: [true, "put number of stocks"]},
        dateAdded: {type: Date, default: new Date()},
        orders: [
            {
                userId: {type: String, required:[true, "UserId is required"]},
                orderAmount: {type: Number},
                orderDate: {type: Date, default: new Date()},
            }
        ]
    }
)

module.exports = mongoose.model("products", productsSchema)