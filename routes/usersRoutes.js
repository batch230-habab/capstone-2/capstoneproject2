const express = require("express");
const router = express.Router();
const usersControllers = require("../controllers/usersControllers");
const productsControllers = require("../controllers/productsControllers");
const auth = require("../auth")

router.post("/register", usersControllers.registerUsersController);

router.post("/login", usersControllers.loginUser);

router.get("/userDetails", auth.verify, usersControllers.getUserDetails);

router.post("/addToCart", auth.verify, usersControllers.addToCart);

router.post("/finalCheckout", auth.verify, usersControllers.finalCheckout);
module.exports = router;