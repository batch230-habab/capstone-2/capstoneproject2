const express = require("express");
const router = express.Router();
const productsControllers = require("../controllers/productsControllers");
const auth = require("../auth.js");

router.post("/newProduct", auth.verify, productsControllers.addProduct);

router.get("/availableProducts", productsControllers.getAllActive);

router.get("/:productId", productsControllers.getProduct);

router.post("/:productId/update", productsControllers.updateProduct);

router.post("/:productId/archive", productsControllers.archiveProduct);

module.exports = router;