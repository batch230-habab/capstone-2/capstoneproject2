const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const usersRoutes = require("./routes/usersRoutes");
const productsRoutes = require("./routes/productsRoutes");
const products = require("./models/products");

const app = express();

app.use(cors());
app.use(express.json());
app.use("/users",usersRoutes)
app.use("/products", productsRoutes);
mongoose.connect("mongodb+srv://admin:admin@batch230.dzsjady.mongodb.net/Habab-eCommerce?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.once("open", () => console.log("Now connected to Mori's Mementos DB"))
app.listen(process.env.PORT || 4000, () =>{
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});